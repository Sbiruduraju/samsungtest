git clone https://https://gitlab.com/Sbiruduraju/samsungtest.git

cd AWS-EKS-Cluster

Terraform EKS Cluster with Istio Ingress

Following components would be created with this script below

A new VPC with multi-zone public & private Subnets, and a single NAT gateway.
A Kubernetes Cluster, based on Spot EC2 instances running in private Subnets, with an Autoscaling Group based on average CPU usage.
An Elastic Load Balancer (ELB) to accept public HTTP calls and route them into Kubernetes nodes, as well as run health checks to scale Kubernetes services if required.
An Istio Ingress Gateway inside the Cluster, to receive & forward HTTP requests from the outside world into Kubernetes pods.
A DNS zone with SSL certificate to provide HTTPS to each Kubernetes service.
A sample application to deploy into our Cluster, using a small Helm Chart.

The very first step in Terraform is to define Terraform configurations, related to state file backend and version to be used in terraform.tf
Recommendation: It is a good idea to declare the version of Terraform to be used while coding our Infrastructure, to avoid any breaking changes that could affect to our code if we use newer/older versions when running terraform in the future.

Recommendation: Backend configuration is almost empty, and that is in purpose. It is recommended to externalize this setup to several files if required (e.g. having one config per environment). In this case we will use a single S3 backend, with several state files for each terraform workspace:

Update the backend.tfvars with the details of S3 bucket name, region, key & workspace_key_prefix
To initialize each workspace, for instance “development”, we should run the following commands:
terraform init
terraform workspace new development
Running this command below will prompt us to enter the values mentioned in the file extensions .tfvars
terraform init -backend-config=backend.tfvars 

In future executions, we can select our existing workspace using the following command:

terraform workspace select development
terraform init -backend-config=backend.tfvars
Define provider.tf with versions
Install and configure AWS CLI and run the command below
aws configure
Enter the details mentioned below
AWS_ACCESS_KEY_ID=AKIAXXXXXXXXXXXXXXXX
AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
AWS_DEFAULT_REGION=us-west-2

Now, we’re ready to start writing our Infrastructure as code!.

Let’s start by creating a new VPC to isolate our EKS-related resources
-->Create network.tf and this includes the code for new VPC with subnets on each Availability Zone with a single NAT Gateway
For this we'll have to define some variable values file and name it network-development.tfvars

Now, we should be ready to create this VPC resources using Terraform. If we already ran init command, we can examine the resources to be created or updated by Terraform using plan command:

execute below
terraform plan -out=development.tfplan -var-file=network-development.tfvars
And then, we can apply those changes using apply command, after user confirmation:

execute below
terraform apply development.tfplan

The next move is to create a new Kubernetes Cluster:

Create a file eks.tf and add the code for creating EC2 autoscaling group for Kubernetes, composed by Spot instances autoscaled out/down based on CPU average usage.
An EKS cluster, with two groups of users (called “admins” and “developers”). 
An EC2 Spot termination handler for Kubernetes, which takes care of reallocating Kubernetes objects when Spot instances get automatically terminated by AWS. This installation uses Helm to ease things up.

And we also define some Kubernetes/Helm Terraform providers, to be used later to install & configure stuff inside our Cluster using Terraform code.

Terraform configuration block uses some variables for previous files
Create a file with name eks-development.tfvars for entering values

to facilitate code reading and an easy variable files usage, it is a good idea to create a separate Terraform configuration file to define all variables at once (e.g. variables.tf) and then define several variable values files as:

execute below
terraform plan -out=development.tfplan -var-file=network-development.tfvars -var-file=eks-development.tfvars

Enter the values as prompted from the vars file

Once the plan is applied, we have a brand-new EKS cluster in AWS!.


-->Now we can move on creating an Elastic Load Balancer (ELB), to handle HTTP requests to our services. The creation of the ELB will be handled by a new Kubernetes Service deployed through a Helm Chart of Istio Ingress deployment:

Create Ingress.tf file which includes AWS-issued SSL certificate to provide HTTPS in our ELB to be put in front of our Kubernetes pods, and also defines some annotations required by Istio Ingress for EKS. At the end it creates a new DNS entry associated with the ELB, which in this example depends on a manually-configured DNS Zone in Route53.

As well as other Terraform configuration files, this one also uses some new variables. So, let’s define them for our “development” environment:

execute below
terraform plan -out=development.tfplan -var-file=network-development.tfvars -var-file=eks-development.tfvars -var-file=ingress-development.tfvars

Enter the values from the vars file when prompted

execute below
terraform apply development.tfplan

The next step is to create some DNS subdomains associated with our EKS Cluster, which will be used by the Ingress Gateway to route requests to specific applications using DNS subdomains:

Create subdomains.tf 


execute below
terraform plan -out=development.tfplan -var-file=network-development.tfvars -var-file=eks-development.tfvars -var-file=ingress-development.tfvars -var-file=subdomains-development.tfvars

Enter the value when prompted from vars

execute below
terraform apply development.tfplan

The next step is to define some Kubernetes namespaces to separate our Deployments and have better management & visibility of applications in our Cluster:
Create namespaces.tf
execute below
terraform plan -out=development.tfplan -var-file=network-development.tfvars -var-file=eks-development.tfvars -var-file=ingress-development.tfvars -var-file=subdomains-development.tfvars -var-file=namespaces-development.tfvars

Enter the values when prompted from vars file
terraform apply development.tfplan

--> The last step is to set up RBAC permissions for the developers group defined in our EKS Cluster:

Create iam.tf
This file is to grant access to see some Kubernetes objects (like pods, deployments, ingresses and services) as well as executing commands in running pods and create proxies to local ports.

execute below
terraform plan -out=development.tfplan -var-file=network-development.tfvars -var-file=eks-development.tfvars -var-file=ingress-development.tfvars -var-file=subdomains-development.tfvars -var-file=namespaces-development.tfvars

execute below
terraform apply development.tfplan


EKS Cluster is fully setup

kubectl get nodes

kubectl run helloworld --image=k8s.gcr.io/echoserver:1.4 --port=8080

kubectl get deployments

kubectl get pods

kubectl expose deployment helloworld --type=LoadBalancer

kubectl describe services helloworld